﻿using DancingGoat.Controllers.Widgets;
using DancingGoat.Models.Widgets.LinkButtonWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("LinkButtonWidget", typeof(LinkButtonWidgetController), "Link Button widget", Description = "Set the text of link button", IconClass = "icon-w-link")]


namespace DancingGoat.Controllers.Widgets
{
    public class LinkButtonWidgetController : WidgetController<LinkButtonWidgetProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();
            var viewModel = new LinkButtonWidgetViewModel
            {
                LinkText = properties.LinkText,
                LinkCSSClass = properties.LinkCSSClass,
                LinkURL = properties.LinkURL,
                LinkTarget = properties.LinkTarget
            };

            return PartialView("Widgets/_LinkButtonWidget", viewModel);
        }
    }
}