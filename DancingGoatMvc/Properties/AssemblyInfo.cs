using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DancingGoat")]
[assembly: AssemblyDescription("A sample ASP.NET MVC 5 application that demonstrates integration with Kentico.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("58482714-3d36-432d-8ba1-c9a882caaa32")]

[assembly: AssemblyProduct("DancingGoat")]
[assembly: AssemblyCopyright("© 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersion("12.0.7115.13953")]
[assembly: AssemblyInformationalVersion("12.0.29")]

[assembly: InternalsVisibleTo("DancingGoat.Tests")]
