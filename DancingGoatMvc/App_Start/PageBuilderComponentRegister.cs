﻿using DancingGoat.Models.PageTemplates;
using DancingGoat.Models.Sections;
using DancingGoat.Models.Widgets;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc.PageTemplates;

// Widgets
[assembly: RegisterWidget("DancingGoat.General.TextWidget", "{$dancinggoatmvc.widget.text.name$}", typeof(TextWidgetProperties), Description = "{$dancinggoatmvc.widget.text.description$}", IconClass = "icon-l-text")]
[assembly: RegisterWidget("DancingGoat.LandingPage.TestimonialWidget", "{$dancinggoatmvc.widget.testimonial.name$}", typeof(TestimonialWidgetProperties), Description = "{$dancinggoatmvc.widget.testimonial.description$}", IconClass = "icon-right-double-quotation-mark")]

// Sections
[assembly: RegisterSection("SingleColumnSection", "{$dancinggoatmvc.section.singlecolumn.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.singlecolumn.description$}", IconClass = "icon-square")]
[assembly: RegisterSection("TwoColumnSection", "{$dancinggoatmvc.section.twocolumn.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.twocolumn.description$}", IconClass = "icon-l-cols-2")]
[assembly: RegisterSection("ThreeColumnSection", "{$dancinggoatmvc.section.threecolumn.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.threecolumn.description$}", IconClass = "icon-l-cols-3")]
[assembly: RegisterSection("CenterContentSection", "{$dancinggoatmvc.section.centercontent.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.centercontent.description$}", IconClass = "icon-l-cols-3")]
[assembly: RegisterSection("FooterSection", "{$dancinggoatmvc.section.footer.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.footer.description$}", IconClass = "icon-square")]
[assembly: RegisterSection("HeaderSection", "{$dancinggoatmvc.section.header.name$}", typeof(ThemeSectionProperties), Description = "{$dancinggoatmvc.section.header.description$}", IconClass = "icon-square")]

// Page templates
[assembly: RegisterPageTemplate("DancingGoat.LandingPageSingleColumn", "{$dancinggoatmvc.pagetemplate.landingpagesinglecolumn.name$}", typeof(LandingPageSingleColumnProperties), Description = "{$dancinggoatmvc.pagetemplate.landingpagesinglecolumn.description$}")]
