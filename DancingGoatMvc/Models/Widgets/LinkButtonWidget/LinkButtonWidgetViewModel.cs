﻿namespace DancingGoat.Models.Widgets.LinkButtonWidget
{
    public class LinkButtonWidgetViewModel
    {
        public string LinkText { get; set; }
        public string LinkCSSClass { get; set; }
        public string LinkURL { get; set; }
        public string LinkTarget { get; set; }
    }
}