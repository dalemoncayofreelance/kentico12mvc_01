using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DancingGoat.Tests")]
[assembly: AssemblyDescription("Tests a sample ASP.NET MVC 5 application that demonstrates integration with Kentico.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("82f9ea9f-daab-4841-b522-d0c1e62b02ef")]

[assembly: AssemblyProduct("DancingGoat.Tests")]
[assembly: AssemblyCopyright("© 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersion("12.0.7115.13953")]
[assembly: AssemblyInformationalVersion("12.0.29")]
